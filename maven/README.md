# Maven

Provides a Java Development Kit and Maven.

## TL;DR

```bash
$ docker run -it registry.gitlab.com/av1o/base-images/maven:16
```

## Supported tags

* `17`, `17-v3`, `17-v3.8.5`
* `18`, `18-v3`, `18-v3.8.5`
* `19`, `19-v3`, `19-v3.8.7`