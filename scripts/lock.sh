#!/usr/bin/env bash

set -eu

START=$(pwd)

function lock() {
  cd "$START/$1"
  ayb lock -c build.yaml
  cd "$START"
}

lock "go/1.22-debian"
lock "go/1.23-debian"
lock "node/20"
lock "python/3.12"
lock "scratch"
