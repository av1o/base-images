# Gradle

Provides a Java Development Kit and Gradle.

## TL;DR

```bash
$ docker run -it registry.gitlab.com/av1o/base-images/gradle:16
```

## Supported tags

* `16`, `16-v7`, `16-v7.2`