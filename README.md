# Base Images

These OCI images serve as base images for further application/environment development.
New images get built every 24 hours to ensure security patches get quickly applied.

*Note: these images are build with [Kaniko](https://github.com/GoogleContainerTools/kaniko) NOT BuildKit.*


## Images

All images try to meet the following criteria:

* Run as a non-root user by default
* Configure non-root usage where possible (e.g. `pip` user-mode)
* Warning when running as root (id=0)
* Based on Alpine where possible

For more information about a specific image, read the README inside its folder.